import './globals.css'

export const metadata = {
  title: 'Faith App',
  description: 'Assesment Landing page for Reactjs Dev at Faith App',
  openGraph: {
    images: [
      {
        url: 'https://opengraph.b-cdn.net/production/documents/4136db01-03f0-4c81-bb5e-eb0054006d5a.png?token=3HSNw2pjwtAxl4y-xSACOHYVurV4wVISxPx7IEwRqUo&height=619&width=1200&expires=33240301112',
        alt: 'ZigSign opengraph Image',
      },
    ],
  },
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  )
}