"use client"
import Image from 'next/image'
import { HeaderImg, Courseinfo, Footer } from '@/Components'

export default function Home() {
  return (
   <>
   <HeaderImg/>
   <Courseinfo/>
   <Footer/>

   </>
  )
}
