import React, { useState, useEffect } from 'react';
// import Tabs from '../components/Tabs';
import videopng from '../../Assets/video.png';
import Image from 'next/image';
import Author from '../../Assets/author.jpg'
import Link from 'next/link';
import jsonData from '../../public/data.json';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter, faFacebook, faInstagram, faYoutube } from '@fortawesome/free-brands-svg-icons';

const Page = () => {
    const [data, setData] = useState(null);

    useEffect(() => {
        setData(jsonData);
    }, []);

    if (!data) {
        return <div></div>;
    }

    const { testimonial, about_instructor, course } = data;


    return (
        <>
            {/* about the course section */}
            <section className='flex sm:flex-col-reverse'>
                <section className='w-3/5 pb-16 pt-4 px-4 sm:w-full'>
                    <div className='pl-32 pt-12 pr-4 sm:pl-12'>
                        <h3 style={{ fontFamily: 'var(--head-font)' }} className='text-4xl w-3/6 pb-4 font-semibold sm:w-full'>
                            About the course
                        </h3>
                        <div className='text-lg' dangerouslySetInnerHTML={{ __html: course.about.html_content }} />
                    </div>
                    <div className='pl-32 pt-8 pr -4 sm:pl-12'>
                        <h3 style={{ fontFamily: 'var(--head-font)' }} className='text-4xl pb-4 font-semibold '>
                            What to expect from the course
                        </h3>
                        <div className='text-lg' dangerouslySetInnerHTML={{ __html: course.what_to_expect.html_content }} />
                    </div>
                    <div className='pl-32 pt-8 pr-4 sm:pl-12'>
                        <h3 style={{ fontFamily: 'var(--head-font)' }} className='text-4xl w-3/6 pb-4 font-semibold sm:w-full'>
                            Key topics covered            </h3>
                        <div className='text-lg' dangerouslySetInnerHTML={{ __html: course.key_topics.html_content }} />
                    </div>
                </section>




                {/* course card pricing box */}
                <section className='w-2/5 z-10 sm:w-full sm:flex sm:justify-center'>
                    <div style={{ background: '#f9f5ff', marginTop: '-80px' }} className='w-4/5 p-12 sm:mt-0'>
                        <div>
                            <h4 className='text-2xl font-semibold sm:text-lg'>
                                Course Fees
                            </h4>
                            <h3 style={{ fontFamily: 'var(--head-font)' }} className='text-6xl font-semibold sm:text-5xl'>
                                ₹{course.fee.amount}
                            </h3>
                        </div>
                        <div className='pt-8'>
                            <h3 className='text-2xl font-semibold sm:text-lg'>What's included</h3>
                            <ul className='flex justify-start text-base mt-2 sm:text-base'>
                                <Image className='mr-2'
                                    src={videopng}
                                    width={20}
                                    height={20}
                                    alt="Picture of a Video Playing Png"
                                /> {course.inclusions.on_demand_videos} on-demand Videos
                            </ul>
                            <ul className='flex justify-start text-base mt-2 sm:text-base'>
                                <Image className='mr-2'
                                    src={videopng}
                                    width={20}
                                    height={20}
                                    alt="Picture of a Video Playing Png"
                                /> {course.inclusions.live_qa_sessions && '2'} livestream sessions
                            </ul>
                            <ul className='flex justify-start text-base mt-2 sm:text-base'>
                                <Image className='mr-2'
                                    src={videopng}
                                    width={20}
                                    height={20}
                                    alt="Picture of a Video Playing Png"
                                /> {course.inclusions.live_qa_sessions && 'Live Q&A sessions'}
                            </ul>
                            <ul className='flex justify-start text-base mt-2 sm:text-base'>
                                <Image className='mr-2'
                                    src={videopng}
                                    width={20}
                                    height={20}
                                    alt="Picture of a Video Playing Png"
                                /> {course.inclusions.whatsapp_community && 'An active Wp community'}
                            </ul>
                            <div className='text-center pt-12'>
                                <Link href={"/"}> <button style={{ background: "var(--purple)" }} className='font-semibold w-full  p-3 px-8 rounded-3xl text-white text-lg'>Register Today</button> </Link>
                            </div>
                        </div>
                    </div>
                </section>
            </section>


            {/* about the author section */}
            <section className='pb-32 sm:pb-16'>
                <div className='pl-32 pt-8 pr-4 sm:pl-4 '>
                    <h3 style={{ fontFamily: 'var(--head-font)' }} className='text-4xl pb-4 font-semibold sm:text-center'>
                        About the Instructor
                    </h3>
                    <div className='flex pt-4 justify-between items-center pr-32 sm:flex-col sm:pr-0 md:pr-8'>
                        <Image className='mr-2 rounded-full sm:mb-8'
                            src={Author}
                            width={200}
                            height={100}
                            alt="Picture of Nintanand Ji"
                        />
                        <div className='text-lg px-8 sm:text-center' dangerouslySetInnerHTML={{ __html: about_instructor.html_content1 }} />
                        <div className='text-lg px-8 sm:text-center' dangerouslySetInnerHTML={{ __html: about_instructor.html_content2 }} />
                    </div>
                    <div className='pt-4'>
                        <ul className='flex justify-between px-80 content-center sm:items-center sm:flex-col sm:px-8 sm:pt-4 md:px-16'>
                            <li className='flex sm:py-2'><FontAwesomeIcon icon={faTwitter} style={{ color: '#1da1f2', fontSize: '1.5rem', marginRight: '10px' }} /> Twitter</li>
                            <li className='flex sm:py-2'><FontAwesomeIcon icon={faFacebook} style={{ color: '#1877f2', fontSize: '1.5rem', marginRight: '10px' }} /> Facebook</li>
                            <li className='flex sm:py-2'><FontAwesomeIcon icon={faInstagram} style={{ color: '#e6869e', fontSize: '1.5rem', marginRight: '10px' }} /> Instagram</li>
                            <li className='flex sm:py-2'><FontAwesomeIcon icon={faYoutube} style={{ color: '#ff0000', fontSize: '1.5rem', marginRight: '10px' }} /> YouTube</li>
                        </ul>
                    </div>
                </div>
            </section>


            {/* Testimonail sections */}
            <section className='bg-slate-100'>
                <div>
                    <section className="overflow-hidden px-6 py-16">
                        <div className="mx-auto max-w-2xl">
                            <figure className="mt-10">
                                <blockquote className="text-3xl text-center font-semibold text-gray-900 pb-12">
                                <div style={{ fontFamily: 'var(--head-font)' }} dangerouslySetInnerHTML={{ __html: testimonial.text }} /> 
                                </blockquote>

                                <div className='flex justify-center'>
                                    <Image className='mr-4 mt-2 rounded-full'
                                        src={Author}
                                        width={65}
                                        height={50}
                                        alt="Picture of Nintanand Ji"
                                    />
                                    <figcaption>
                                        <div className="mt-4 flex flex-col items-start text-base">
                                            <div dangerouslySetInnerHTML={{ __html: testimonial.reviewer_name }} />
                                            <div dangerouslySetInnerHTML={{ __html: testimonial.reviewer_designation }} />
                                        </div>
                                    </figcaption>
                                </div>
                            </figure>
                        </div>
                    </section>
                </div>
            </section>



        </>
    );
};

export default Page;
