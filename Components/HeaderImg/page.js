"use client"
import React, { useState, useEffect } from 'react';
import Herobg from '../../Assets/hero-bg.jpg';
import jsonData from '../../public/data.json';

const Page = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    setData(jsonData);
  }, []);

  if (!data) {
    return <div>Loading...</div>;
  }

  const { instructor, course } = data;

  return (
    <>
      <mainsection>
        <section
          style={{
            backgroundImage: `linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.1)), url(${Herobg.src})`,
            backgroundPosition: 'top',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            width: '100%',
            height: '550px',
            position: 'relative', // Required for absolute positioning of content
            display: 'flex',
            justifyContent: 'left',
            alignItems: 'end',
            // Add other styles as needed
          }}
        >
          <div className='pb-20 pl-32 sm:pl-12 sm:pb-36'>
            <h4 className='text-white text-xl'>{instructor.name}</h4>
            <h1 style={{ fontFamily: 'var(--head-font)' }} className='text-white text-5xl w-3/6 pt-4 font-semibold sm:w-full sm:text-4xl'>
              {course.title}
            </h1>
          </div>
        </section>
      </mainsection>
    </>
  );
};

export default Page;
