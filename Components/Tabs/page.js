// components/Tabs.js
import React, { useState } from 'react';

const page = ({ defaultTab, tabs }) => {
  const [activeTab, setActiveTab] = useState(defaultTab);

  const handleTabClick = (tab) => {
    setActiveTab(tab);
  };

  return (
    <div>
      <div className="tab-buttons">
        {tabs.map((tab) => (
          <button
            key={tab}
            onClick={() => handleTabClick(tab)}
            className={activeTab === tab ? 'active' : ''}
          >
            {tab}
          </button>
        ))}
      </div>
      <div className="tab-content">
        {tabs.map((tab) => (
          <div key={tab} className={activeTab === tab ? 'active' : 'hidden'}>
            <p>{`Content for ${tab} tab`}</p>
          </div>
        ))}
      </div>
      <style jsx>{`
        .tab-buttons {
          display: flex;
        }
        button {
          padding: 8px 16px;
          margin: 4px;
          cursor: pointer;
          background-color: #f0f0f0;
          border: 1px solid #ccc;
        }
        button.active {
          background-color: #e0e0e0;
        }
        .tab-content div {
          display: none;
        }
        .tab-content div.active {
          display: block;
        }
      `}</style>
    </div>
  );
};

export default page;
